new WOW().init();

//input zoom js
document.documentElement.addEventListener('touchstart', function (event) {
    if (event.touches.length > 1) {
        event.preventDefault();
    }
}, false);


//first letter cap

jQuery('.firstCap').on('keypress', function (event) {
    var $this = jQuery(this),
        thisVal = $this.val(),
        FLC = thisVal.slice(0, 1).toUpperCase();
    con = thisVal.slice(1, thisVal.length);
    jQuery(this).val(FLC + con);
});


     
/*testimonal*/
     
          $(document).ready(function() {
              $('.testimonalBlk .owl-carousel').owlCarousel({
                loop: false,
                  nav:false,
                  dots:true,
                margin: 20,
                responsiveClass: true,
                  
                responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 1

                  },
                  1000: {
                    items: 1
                  }
                }
              })
            })
          
          
// Add smooth scrolling to all links		



/*fixed header*/

jQuery(document).ready(function() {

  var h = jQuery("header");
  var b = jQuery("body");
  var headerHeight = jQuery(".bannerBlock").outerHeight();
  jQuery(window).scroll(function() {
        var windowpos = jQuery(window).scrollTop();

    if (windowpos >= headerHeight) {
      h.addClass("stick animated fadeInDown");
      b.addClass("scrolled");
    } else {
      h.removeClass("stick animated fadeInDown");
      b.removeClass("scrolled");
    }
  });

});    

jQuery(window).on("load resize scroll", function(e) {
    var Header = jQuery("header").innerHeight();
    jQuery('.bannerBlock').css('margin-top',Header); 

});


function mobileElements() {
    if (jQuery(window).width() < 767) {
       
        jQuery('.navbar-collapse').removeClass('show')
    jQuery(".navbar-nav>li>a").attr({"data-toggle":"collapse", "data-target":"#navbarSupportedContent"});
    }

  };

mobileElements();

jQuery(window).on("resize", function (e) {
    mobileElements();
});


jQuery(document).on('click', '.btn', function(e) {

  var index = jQuery(this).attr("href");

  if (jQuery(index).length > 0) {
    jQuery('html,body').animate({
        scrollTop: jQuery(index).offset().top - 55
      },
      3500);

  }
});



jQuery(document).on('click', '.nav-item', function(e) {
    
    e.preventDefault();

     mobileElements();
  jQuery('li').removeClass('current-menu-item');
  jQuery(this).addClass('current-menu-item');
    
  var index         = jQuery(this).find('a').text();    
  var headerClass   = jQuery('header').hasClass('stick');     
  index = (index == "CONTACT US" ? 'CONTACTUS' : index);
     index = (index == "ABOUT US" ? 'ABOUTUS' : index);
  if (jQuery('#'+index).length > 0) {
      if(headerClass){
          jQuery('html,body').animate({
            scrollTop: jQuery('#'+index).offset().top - 43
          },
          3500);
          
      } else {
        if (jQuery(window).width() < 767) {
          jQuery('html,body').animate({
            scrollTop: jQuery('#'+index).offset().top - 175
          },
          3500);
        } else{
          jQuery('html,body').animate({
            scrollTop: jQuery('#'+index).offset().top - 150
          },
          3500);
        }
      }
      
       
  }
   
});

